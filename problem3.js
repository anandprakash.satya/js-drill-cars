// ==== Problem #3 ====
// The marketing team wants the car models listed alphabetically on the website. 
//Execute a function to Sort all the car model names into alphabetical order and log the results in the console as it was returned.


function problem3(inventory){
    let arr=[]
    for(let i=0;i<50;i++){
        let mod=inventory[i].car_model
        let upper=mod.toUpperCase()
        arr.push(upper)
    }
    arr.sort()
    return arr
}

module.exports = {problem3}