// ==== Problem #5 ====
// The car lot manager needs to find out how many cars are older than the year 2000. 
//Using the array you just obtained from the previous problem, 
//find out how many cars were made before the year 2000 and return the array of older cars and log its length.


function problem5(inventory){
    
    let array_year =[]
    for(let i=0;i<50;i++){
        if(inventory[i].car_year<2000){
         
            array_year.push(inventory[i].car_year)
        }
    }
    return array_year
}
module.exports ={problem5}